import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

# setup import of own tools.
# append the path to the folder containing im_tools.py and __init__.py
sys.path.append("/Users/Tom/Documents/Research/code/pyscripts/")
import im_tools

plt.ion()

### The master unwarp.py file is found in /Users/Tom/Documents/Research/code/
# pyscripts/. It is copied into research specific folders so that parameters
# can be changed. ###

# root folders where all the files are
stem_data = Path(
    "/Users/Tom/Documents/Research/Data/2020/2020-12-14_MoS2_Ptychography_for_marcel_higher_dose"
)
stem_results = Path(
    "/Users/Tom/Documents/Research/Results/HU Postdoc/2020-12-14_MoS2_Ptychography_for_marcel_higher_dose"
)

# tilt tableau datasets - ref is ronchicam, warp is dectris. these are 4D stacks
f_ref = stem_data / "Spectrum Image (RonchiCam) - tilt tableau.npy"
f_warp = stem_data / "Spectrum Image (Dectris) - tilt tableau.npy"

# control flow and user defined parameters
# first you find the beam in the reference 4D stack, and then the warped stack
# fit he following flags are set to false, the code expects a text file of beam
# positions in the format 'x_pos y_pos', with each location a new line
find_points_ref = False
find_points_warp = False
dist_min = 20  # how far apart (in pixels) do the detected disks in the ronchigram stack need to be, to be considered a valid disk

# if this does not seem to be working, first see what is going on with the
# following flag set to True. This is slow, so return to false once the
# parameters above and below are found.
plot_flag = True

# there are two more parameters in the code below, the radius used in
# im_tools.mask_circ and the threshhold value for im_tools.com_im. The default
# values should work for standard data
radius_ref = 18
radius_warp = 10
thresh_ref, thresh_warp = 100, 100

# save dewarping parameters once all are successfully found as text files in the folder
save_all = False

# work on and save raw 4D stacks - this is now working on the experimental data
# for ptychography, etc. If you have everything above working, and saved the
# necessary text files, set everything above to False and everything below to
# True, and you will save a new, unwarped, 4D stack
load_data = False
process_data = False
save_data = False

# can now also use the json to extract tilts - unfortunately this is not yet good enough to match the tilts from the ronchigram, so some post-processing is needed to extract tilts.

# x, y = im_tools.get_json_tilts(path_to_json_file)
# these then go into the ab matrix calculation. Their mean must be subtracted.

# raw data stack path - 4D numpy files extracted from Swift. Keep in mind
# stem_data is the base path to the folder defined above.

f = stem_data / "Spectrum Image (Dectris) - 17.42.npy"

### no more user defined inputs below this point ###

ref_im = np.load(f_ref, mmap_mode="r")
warp_im = np.load(f_warp)

# make Dectris data square, to prevent clipping
warp_im, pad_size = im_tools.pad_square(warp_im, axis=(2, 3))

# warp_im /= np.max(warp_im)
# ref_im /= np.max(ref_im)
# ref_im -= np.min(ref_im)


if find_points_ref:
    coords, max_vals = [], []
    for i in tqdm(range(ref_im.shape[0])):
        for j in range(ref_im.shape[1]):
            _ = im_tools.find_disk(
                ref_im[i, j, :, :],
                com_thresh=thresh_ref,
                radius=radius_ref,
                plot_flag=plot_flag,
            )
            coords.append(_[0])
            max_vals.append(_[1])

    max_vals = np.asarray(max_vals)
    coords = np.asarray(coords)
else:
    print(
        f"Loading data from {str(stem_results)}/coords.txt and /max_vals.txt. This will fail if these do not exist."
    )
    coords = np.loadtxt(stem_results / "coords.txt")
    max_vals = np.loadtxt(stem_results / "max_vals.txt")

if find_points_warp:
    coords_warp, max_vals_warp = [], []
    for i in tqdm(range(ref_im.shape[0])):
        for j in range(ref_im.shape[1]):
            _ = im_tools.find_disk(
                warp_im[i, j, :, :],
                com_thresh=thresh_warp,
                radius=radius_warp,
                plot_flag=plot_flag,
            )
            coords_warp.append(_[0])
            max_vals_warp.append(_[1])

    max_vals_warp = np.asarray(max_vals_warp)
    coords_warp = np.asarray(coords_warp)
else:
    print(
        f"Loading data from {str(stem_results)}/coords_warp.txt and /max_vals_warp.txt. This will fail if these do not exist."
    )
    coords_warp = np.loadtxt(stem_results / "coords_warp.txt")
    max_vals_warp = np.loadtxt(stem_results / "max_vals_warp.txt")

# remove bad images by various metrics.
inds = np.ones(max_vals_warp.shape[0]).astype(bool)
inds[max_vals_warp < 0.5 * np.mean(max_vals_warp)] = False
inds[max_vals < np.mean(max_vals)] = False

# minimum distance metric
# set a minimum distance between spots due to ronchiogram ghosting
dist = np.zeros_like(inds).astype(float)
for i in range(len(inds) - 1):
    dist[i + 1] = np.linalg.norm(coords[i + 1, :] - coords[i, :])
# look at the histogram to figure out the correct number
# plt.hist(dist[inds], 100)
inds[dist < dist_min] = False
# inds[dist>1000] = False

# save values from finding disks as text files
if save_all:
    np.savetxt(stem_results / "coords.txt", coords)
    np.savetxt(stem_results / "max_vals.txt", max_vals)
    np.savetxt(stem_results / "coords_warp.txt", coords_warp)
    np.savetxt(stem_results / "max_vals_warp.txt", max_vals_warp)
    np.savetxt(stem_results / "inds.txt", inds)
    np.savetxt(stem_results / "dist.txt", dist)

# fit surface, ab is the distortion matrix
warp_avg = np.mean(warp_im, axis=(0, 1))
ab = im_tools.find_ab(
    coords[inds, 0] - np.mean(coords[inds, 0]),
    coords[inds, 1] - np.mean(coords[inds, 1]),
    coords_warp[inds, 0],
    coords_warp[inds, 1],
)

# this command unwarps the image
im_tools.unwarp_im(warp_avg, ab, plot_flag=True)
plt.pause(0.01)

# maybe still need to fix gain? using original image?  i don't think so

# now we unwarp the 4D stacks of actual data
if load_data:
    data = np.load(f, mmap_mode="r")

if process_data:
    data = np.pad(
        data, pad_size
    )  # this is perhaps unnecessary now that my padding function can handle 2D images. This step takes a lot of memory
    data_mean = np.mean(data, axis=(0, 1))
    print("unwarping real data")
    for i in tqdm(range(data.shape[0])):
        for j in range(data.shape[1]):
            data[i, j, :, :] = im_tools.unwarp_im(
                data[i, j, :, :], ab, plot_flag=False
            )[0]

    data_unwarp_mean = np.mean(data, axis=(0, 1))

    plt.figure(34, clear=True)
    plt.imshow(data_unwarp_mean ** 0.25)

if save_data:
    np.save(f.parent / (f.stem_data + "_unwarped.npy"), data)
