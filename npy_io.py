import numpy as np
import struct


def npy_writer(path, path_out):
    """
    This function will simply take a npy file and return a bin file for ptyIDES
    """

    data = np.load(path)
    data /= np.max(np.sum(data, axis=(2, 3)))
    data = np.transpose(data, (1, 0, 2, 3))
    data = np.ravel(data)

    # scale the data

    data = struct.pack(data.shape[0] * "f", *data)
    f = open(path_out, "wb")
    f.write(data)
    f.close()

    return
