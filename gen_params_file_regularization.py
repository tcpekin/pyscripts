import sys
import argparse
import os
import numpy as np
from pathlib import Path
from pprint import pprint
from ides_tools import gen_beam_positions
from string import Template


def gen_params_file(output_file_name, mu):
    """
    This file will generate a parameter file. It will deal with regularization values, hardcoded to run 19 jobs, logarithmically spaced from mu = 0.00001 to 10. 
    """
    file_dir = Path(__file__).parent
    param_template = (file_dir / "params_template_regularization.cnf").resolve()
    with open(param_template) as f:
        params = Template(f.read())

    # generate mu values - want the range to span from 0.01 to 10 in logarithmic spacing
    values = np.logspace(-5, 1, 19)  # 6 orders of magnitude * division + 1
    mu = values[int(mu)]

    # make dictionary and substitute
    d = {"mu": mu}
    params_file = params.substitute(d)
    output_file_name += ".cnf"
    with open(output_file_name, "w") as f_out:
        f_out.write(params_file)
    print(os.path.abspath(output_file_name))
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a parameter file.")
    parser.add_argument("output_file_name", type=str, help="The output file name")

    parser.add_argument(
        "mu",
        type=float,
        help="the mu value to be fed into an exponential spacing function",
    )

    args = parser.parse_args()
    # pprint(vars(args), width=50)
    gen_params_file(**vars(args))
