This is a collection of scripts I find helpful in Python. Likely, im_tools and ides_tools are the most useful. They can be imported using 

```
# setup import of own tools
import sys

sys.path.append("/Users/Tom/Documents/Research/code/pyscripts/")
import im_tools
```

where whatever path you have the file im_tools should be used above. Then it works like a normal package, i.e. `im_tools.unwarp_im(im, ab)`.

Code is written to be used in IPython command prompts, or in scripts run through IPython. It should work in Jupyter notebooks, but no assurances are made. If the two following lines are executed before loading any packages, any changes you make to the code is then updated in your IPython instance the next time it is run. This is helpful for debugging/iterating code.

```
load_ext autoreload
autoreload 2
```

Most objects used are just numpy arrays. 

Code has attribution in the function docstring if written by others. This is Wouter Van den Broek with the unwarping code for the Dectris detector, and Alberto Eljarrat Ascunce for the Nion library reader. 

Contact: [tcpekin@gmail.com](mailto:tcpekin@gmail.com)