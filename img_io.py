import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
from scipy import signal
import struct
from tqdm import tqdm

""" 
This file will have code to read .img files directly from Christoph Koch's
.img format. It will then be able to loop through all available files 
(constrained to specific path format), and generate a large .bin file that can 
be fed into IDES.
"""


def img_read(fname, print_flag=1, plot_flag=1):
    """
    Image reader adapted from Matlab for Christoph's .img data
    """

    # header = [header_size(bytes), param_size, comment_size, Nx, Ny,
    # compl_flag, double_flag, data_size, version] header_length = 4

    fid = open(fname, "rb")
    # vals = fid.read() header = struct.unpack('<iiiiiiii', vals[0:32]) the
    # pointer will be moving this way instead of reading everything
    header = struct.unpack("<iiiiiiii", fid.read(32))
    Nx = header[4]
    Ny = header[3]
    im_size = Nx * Ny
    t = struct.unpack("<d", fid.read(8))[0]
    dx = struct.unpack("<d", fid.read(8))[0]
    dy = struct.unpack("<d", fid.read(8))[0]

    if print_flag:
        print("Nx, Ny = ({}, {})".format(Nx, Ny))
        print("t, dx, dy = ({:.2f}, {:.2f}, {:.2f})".format(t, dx, dy))

    # read in additional parameters from file, if any exist:
    param_size = header[1]
    if param_size > 0:
        params_fmt = "<" + param_size * "d"
        params = struct.unpack(params_fmt, fid.read(param_size * 8))
        if print_flag:
            print("disabled parameters printing")
            # print('Parameters:\n{}'.format(params))

    # read comments from file, if any exist
    comment_size = header[2]
    if comment_size > 0:
        comment_fmt = "<" + str(comment_size) + "s"
        comment = struct.unpack(comment_fmt, fid.read(comment_size))[0]
        comment = comment.decode("utf-8")
        if print_flag:
            print(comment)

    integer_flag = 0
    complex_flag = header[5]
    # this needs to be checked, Matlab code was doubleFlag = (header(7) ==
    # 8*(complexFlag+1));
    double_flag = header[6] == 8 * (complex_flag)

    flag = integer_flag + double_flag * 4 + complex_flag * 2
    if print_flag:
        print("flag = {}".format(flag))

    # bit 2: doubld bit 1: complex bit 0: integer

    if print_flag:
        print("img_read {}: {} x {} pixels, flag = {}".format(fname, Nx, Ny, flag))

    flag = flag % 8

    if flag == 0:
        complex_flag = 0
        double_flag = 0
        im_fmt = "<" + Nx * Ny * "f"
        if print_flag:
            print("32-bit real data, {:.3f}MB".format(im_size * 4 / 1048576))
        img = struct.unpack(im_fmt, fid.read(im_size * 4))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, Nx])
    elif flag == 1:
        print("NO guarantees that this works!!")

        complex_flag = 0
        double_flag = 0
        im_fmt = "<" + Nx * Ny * "h"
        if print_flag:
            print("16-bit integer data, {:.3f}MB".format(im_size * 2 / 1048576))
        img = struct.unpack(im_fmt, fid.read(im_size * 2))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, Nx])
    elif flag == 2:
        print("NO guarantees that this works!!")
        complex_flag = 1
        double_flag = 0
        im_fmt = "<" + 2 * Nx * Ny * "f"
        if print_flag:
            print("32-bit complex data, {:.3f}MB".format(im_size * 8 / 1048576))
        img = struct.unpack(im_fmt, fid.read(2 * im_size * 4))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, 2 * Nx])
        img = img[:, ::2] + 1j * img[:, 1::2]
    elif flag == 4:
        print("NO guarantees that this works!!")
        complex_flag = 0
        double_flag = 1
        im_fmt = "<" + Nx * Ny * "d"
        if print_flag:
            print("64-bit real data, {:.3f}MB".format(im_size * 8 / 1048576))
        img = struct.unpack(im_fmt, fid.read(im_size * 8))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, Nx])
    elif flag == 5:
        print("NO guarantees that this works!!")
        complex_flag = 0
        double_flag = 0
        im_fmt = "<" + Nx * Ny * "i"
        if print_flag:
            print("32-bit int data, {:.3f}MB".format(im_size * 4 / 1048576))
        img = struct.unpack(im_fmt, fid.read(im_size * 4))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, Nx])
    elif flag == 6:
        print("NO guarantees that this works!!")
        complex_flag = 1
        double_flag = 1
        im_fmt = "<" + 2 * Nx * Ny * "d"
        if print_flag:
            print("64-bit complex data, {:.3f}MB".format(im_size * 16 / 1048576))
        img = struct.unpack(im_fmt, fid.read(2 * im_size * 8))
        img = np.asarray(img)
        img = np.reshape(img, [Ny, 2 * Nx])
        img = img[:, ::2] + 1j * img[:, 1::2]

    if plot_flag:
        plt.figure(12)
        plt.clf()
        # for diffraction patterns, will throw divide by zero error
        plt.imshow(np.log(img))
        plt.title("log of data!")
        # plt.imshow(img) # for detector images
        plt.show()
    fid.close()
    return img


def import_all_img(
    path,
    ysize=165,
    xsize=51,
    crop_dim=0,
    file_suffix="",
    scale_factor=63963870.573019154,
):
    """
    This code will be given a top level path, a scan size, crop dimensions, file suffix, and scale factor, and will output a binary file with the images arranged in the correct order.

    :param path: this is a string to the top level folder containing all of the .img files
    :param ysize: an integer for number of diffraction patterns in the y (vertical) direction
    :param xsize: an integer for number of diffraction patterns in the x (vertical) direction
    :param crop_dim: (x_left, x_right, y_left, y_right) where to crop the images
    :param suffix: string to append to the filename 'IntensityIncSum'
    :param scale_factor: number to divide all pattern values by - usually chosen by summing an entire pattern from the center of the scan.
    :returns: Nothing
    """
    # this line had 'ab' as the write option, for append, but then if you ran
    # the code two times in a row it would concatenate the files - not wanted
    testing = 0
    file_out = open(path + "IntensityIncSum" + file_suffix + ".bin", "wb")
    for i in tqdm(range(ysize)):
        for j in range(xsize):
            data = img_read(
                path + "/img/" + "diffAvg_" + str(i) + "_" + str(j) + ".img", 0, 0
            )
            if crop_dim != 0:
                data = data[crop_dim[0] : crop_dim[1], crop_dim[2] : crop_dim[3]]

            if testing:
                plt.figure(11)
                plt.clf()
                plt.imshow(np.log(data))
                plt.show
                return

            data = np.ravel(data) / scale_factor
            N = data.shape
            # the *data stretches out the data so that it all gets packed
            data = struct.pack(np.prod(N) * "f", *data)
            file_out.write(data)
    file_out.close()
    return


# run code
plt.ion()
path_detector = (
    "/media/tom/Data/Ptychography_Simulation/Si_dislo_TDS/img/detector2_4.img"
)
path_CBED = (
    "/media/tom/Data/Ptychography_Simulation/Si_dislo_noTDS/img/diffAvg_82_23.img"
)
path_top = "/media/tom/Data/Ptychography_Simulation/Si_dislo_noTDS/"

path_YAO = "/media/tom/Data/Berlin_YAO/20181114_Pty_Merlin/bin/"


# im = img_read(path_detector)

# import_all_img(path_top, file_suffix='uncropped')
# import_all_img(path_top, file_suffix='50px_crop', crop_dim=(50,-50,50,-50))
# import_all_img(path_top, crop_dim=(150, -150, 150, -150), file_suffix='150px_crop')


# import_all_img(path_top)
