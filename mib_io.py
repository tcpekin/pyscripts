import os
import struct
import copy
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from im_tools import bin_image, com_im

plt.ion()


def mib_reader(fname, bin_factor=(1, 1), write_bin=None, data_type_out=None, rot=None):
    """
    This function imports .mib files, and can write binary files from the data. It can also bin the images, as well as center and rotate the patterns if rot is specified. 

    :param fname: the .mib file to read
    :param bin_factor: length 2 tuple of how much to bin in the (x, y) directions
    :param write_bin: writes binary file to the filename specified by write_bin if not None
    :param data_type_out: sets the data size to write to. Should fix this to be more robust in the future.
    :param rot: an angle in degrees to rotate the diffraction patterns by counterclockwise. Any value that is not None will work, as if the angle is 0, the patterns will still be centered.
    """
    file = open(fname, "rb")
    file_size = os.fstat(file.fileno()).st_size

    header_offset = 16
    header = file.read(header_offset)  # hard coded to get this many bytes
    header = header.decode("utf-8").split(",")
    header_size = int(header[2])
    header += file.read(header_size - header_offset).decode("utf-8").split(",")[1::]

    im_size = [int(header[4]), int(header[5])]
    # this will fail when the image is a binary image and only has one byte
    bytes_per_pixel = int(header[6][1:]) // 8
    if bytes_per_pixel == 1:
        data_type = np.dtype(np.uint8)
        data_char = "B"
    elif bytes_per_pixel == 2:
        data_type = np.dtype(np.uint16)
        data_char = "H"
    elif bytes_per_pixel == 4:
        data_type = np.dtype(np.uint32)
        data_char = "I"
    elif bytes_per_pixel == 8:
        data_type = np.dtype(np.uint64)
        data_char = "L"

    num_images = file_size // (im_size[0] * im_size[1] * bytes_per_pixel + header_size)

    if num_images % 1 != 0:
        print(num_images)
        raise ValueError("you have computed the number of images you have wrongly")

    im_size_old = copy.deepcopy(im_size)
    if all([i != 1 for i in bin_factor]):
        im_size = np.true_divide(im_size, bin_factor)
        im_size = [int(i) for i in im_size]
        # force a float b/c we will be doing arithmatic
        data_type_out = np.dtype(np.float32)

    if data_type_out != None:
        data_type_out = np.dtype(data_type_out)
        scaling_factor = data_type_out.itemsize / data_type.itemsize
        data_type = data_type_out
    else:
        # default to float32 - what PIDES reads
        data_type_out = np.dtype(np.float32)
        scaling_factor = data_type_out.itemsize / data_type.itemsize
        data_type = data_type_out

    print(
        "Importing {} images as a {} ({} GB) ...\n".format(
            num_images,
            data_type,
            num_images * np.prod(im_size) * bytes_per_pixel * scaling_factor / 1e9,
        )
    )

    data = np.zeros(im_size + [num_images], dtype=data_type)

    for i in tqdm(range(num_images)):
        im = file.read(np.prod(im_size_old) * bytes_per_pixel)
        im = struct.unpack(">" + data_char * np.prod(im_size_old), im)
        im = np.asarray(im, data_type)
        # can enforce matlab ordering here by adding 'F' - This should not be necessary as I think PIDES just requires the same bytes read in to be written out
        # print(im.dtype)
        # im = bin_image(np.reshape(im, im_size_old, order='F'), bin_factor)
        im = bin_image(np.reshape(im, im_size_old), bin_factor)
        data[:, :, i] = im
        file.read(header_size)  # skip the next header

    # scale data
    # data = data.astype('float32')
    print(f"The data is being saved as {data.dtype}.")
    # The expected value of the central disk should be 100 for error metric 4
    central_disk_px = np.sum(np.mean(data, 2) > (np.mean(data) + np.std(data)))
    data = data / (np.sum(data[:, :, int(data.shape[2] / 2)])) * central_disk_px * 100

    # rotate/center data
    if rot != None:
        # center to mean CBED
        N = data.shape
        data_mean = np.mean(data, 2)
        x_cm, y_cm = com_im(data_mean)
        x_shift, y_shift = np.add([i / 2 for i in N[0:2]], [-x_cm, -y_cm])
        data = np.roll(data, (int(y_shift), int(x_shift), 0), axis=(0, 1, 2))
        com_im(np.mean(data, 2))
    file.close()
    if write_bin != None:
        bin_write(data, write_bin)

    return data


# fname = '/Users/Tom/Desktop/DataJuelich_20181010-12/20181012/MTF/MTF_1_.mib'
# # fname_out = '/Users/Tom/Desktop/DataJuelich_20181010-12/20181012/MTF_1_.bin'
# fname = "/media/tom/Data/Berlin_YAO/20181114_Pty_Merlin/M2-50_YAO/M2-50_YAO_1.mib"
# fname = "/media/tom/Data/Berlin_YAO/20181114_Pty_Merlin/M5_YAO/M5_YAO_1.mib"
# # fname_out = '/media/tom/Data/Berlin_YAO/20181114_Pty_Merlin/bin/M2-50_YAO_1_origorder.bin'
# fname1 = "/mnt/tompekin/Berlin_penghan_YAlO3_Ce/20190330_Pty_Merlin/YAO/1.mib"
# fname2 = "/mnt/tompekin/Berlin_penghan_YAlO3_Ce/20190330_Pty_Merlin/YAO/2.mib"
# fname3 = "/mnt/tompekin/Berlin_penghan_YAlO3_Ce/20190330_Pty_Merlin/YAO/3.mib"

# data = mib_reader(fname, write_bin=None, bin_factor=(2,2))
# data_rot = mib_reader(fname, write_bin=fname_out, bin_factor=(2, 2), rot=0)
# data1 = mib_reader(
#     fname1, write_bin=None, bin_factor=(1, 1), rot=0, data_type_out=np.dtype(np.float32)
# )
# data2 = mib_reader(
#     fname2, write_bin=None, bin_factor=(1, 1), rot=0, data_type_out=np.dtype(np.float32)
# )
# data3 = mib_reader(
#     fname3, write_bin=None, bin_factor=(1, 1), rot=0, data_type_out=np.dtype(np.float32)
# )

# com_im(np.mean(data, 2))
