import sys
import argparse
import os
from pathlib import Path
from pprint import pprint
from ides_tools import gen_beam_positions
from string import Template


def gen_params_file(output_file_name, scan_size, step_size, rot, defocus):
    """
    This file will generate a parameter file. It will eventually be built up, but initially it will only deal with beam positions
    """
    file_dir = Path(__file__).parent
    param_template = (file_dir / "params_template_probe_positions.cnf").resolve()
    with open(param_template) as f:
        params = Template(f.read())

    # generate beam positions
    beam_pos = gen_beam_positions(scan_size, step_size, rot, write_txt=False)
    beam_pos = [
        "beam_position: {:.6e}    {:.6e} \n".format(beam[0], beam[1])
        for beam in beam_pos
    ]
    beam_pos = "".join(beam_pos)

    # make dictionary and substitute
    d = {"beam_positions": beam_pos, "defocus": f"{(defocus/4)-16}e-9"}
    params_file = params.substitute(d)
    output_file_name += ".cnf"
    with open(output_file_name, "w") as f_out:
        f_out.write(params_file)
    print(os.path.abspath(output_file_name))
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a parameter file.")
    parser.add_argument("output_file_name", type=str, help="The output file name")
    parser.add_argument(
        "scan_size",
        type=int,
        nargs=2,
        help="The scan size in number of real space probes (2D, input as sx sy)",
    )
    parser.add_argument(
        "step_size",
        type=float,
        help="The step size between adjacent pixels - assumes square pixels",
    )
    parser.add_argument(
        "rot",
        type=float,
        help="The rotation angle between the diffraction pattern and the scan in degrees",
    )
    parser.add_argument("defocus", type=float, help="The defocus in nanometers + 15")

    args = parser.parse_args()
    # pprint(vars(args), width=50)
    gen_params_file(**vars(args))
