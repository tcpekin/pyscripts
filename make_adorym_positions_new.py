import numpy as np
from matplotlib import pyplot as plt

N_scan_x=128
N_scan_y=128
scan_step_size_x_m=0.3125 * 1e-10 
scan_step_size_y_m=0.3125 * 1e-10 
rot_ang=np.pi/180*(-173.70)
px_size_m = 1.035809e-11

ppx = np.arange(N_scan_x)
ppy = np.arange(N_scan_y)

ppY, ppX = np.meshgrid(ppx, ppy, indexing='ij')

R = np.asarray(
    [[np.cos(rot_ang), -np.sin(rot_ang)],
     [np.sin(rot_ang), np.cos(rot_ang)]]
)

xy = np.vstack((ppX.ravel() * scan_step_size_x_m, ppY.ravel() * scan_step_size_y_m))

print(xy)

xy_rot = R @ xy

plt.figure(10,clear=True)
plt.scatter(xy[0,:], xy[1,:])
plt.scatter(xy_rot[0,:], xy_rot[1,:])

xy_rot /= px_size_m

xy_rot = xy_rot -  np.min(xy_rot, axis=1)[:,None]

plt.figure(11, clear=True)
plt.scatter(xy_rot[0,:], xy_rot[1,:], c=np.linspace(0,1,N_scan_x*N_scan_y))
print(xy_rot)

np.save(f'/Users/tompekin/mnt/hbn_2022/adorym/15.28_dataset/beam_pos_{rot_ang*180/np.pi:.2f}_{N_scan_x}_{N_scan_y}.npy', xy_rot.T)

