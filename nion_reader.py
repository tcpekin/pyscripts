"""
Nion Reader
-----------
Alberto Eljarrat, Humboldt University of Berlin 2020. 
aeljarrat@physik.hu-berlin.de

Some scripts to load data from Nion-Swift libraries. A list of dictionaries
object is returned. Each item in this list contains a metadata dictionary,
corresponding to a data element of the Swift library. Some routines are included
to sort this list efficiently.

Example
-------
Loading some Swift library metadata into a list of dictionaries.
>>> fname = "/some/swift/dir"
>>> filedata = get_dict_list(fname)

Sorting the data according to a timestamp key.
>>> timestamp = 'created'
>>> timedata = filedata.sort_by_key(timestamp)

Get some raw EELS data, they are identified by their collection dimension.
>>> eelsdata = timedata.get_by_key('collection_dimension_count', 2)

Get some HAADF data, they are identified by their datum dimension.
>>> hadfdata = timedata.get_by_key('datum_dimension_count', 2)

Get the disk location for these HAADF data, load them to Hyperspy and plot.
>>> files = [d['disk_location'] for d in hadfdata]      # get disk locations ...
>>> images = [load_nion_file(file) for file in files]   # ... load files ...
>>> ims = hs.plot.plot_images(images, per_row=5)        # ... plot as images.
"""

from os import walk
import numpy as np
import json
import h5py
import re

_hyperspy_support = False#True
#try:
#    from hyperspy.signals import BaseSignal, Signal1D, Signal2D
#except ImportError:
#    _hyperspy_support = False
#    raise ImportWarning(
#        "The `hyperspy` package could not be found."
#        + "Some functionality will not be available."
#    )


class DictList(list):
    """
    A list of dictionaries with some sorting methods.

    Methods
    -------
    sort_by_key
    get_by_key
    get_key_values
    """

    def __init__(self, *args, **kwargs):
        """
        Look for files in the given directory and sub-directories
        containing .ndata and .h5 files. Load them lazily and keep
        the json metadata into a list of dictionaries.
        """
        super().__init__(*args, **kwargs)

    def sort_by_key(self, key):
        """
        Returns a sorted list of the elements that contains key.
        """
        out = [d for d in self if key in d]
        out.sort(key=lambda x: x[key])
        return DictList(out)

    def get_key_values(self, key):
        """
        Returns a list of the values in the keys.
        """
        out = [d[key] for d in self]
        return DictList(out)

    def get_by_key(self, key, value):
        """
        Returns a list of elements with the same value in the key.
        """
        out = [d for d in self if d[key] == value]
        return DictList(out)

    def search_by_key(self, key, value):
        """
        Return a list of elements with the value as part of the key.
        """
        out = [d for d in self if value in d[key]]
        return DictList(out)

    def regex_by_key(self, key, regex):
        """
        Return a list of elements whose value matches the provided regex.
        """
        r = re.compile(regex)
        out = [d for d in self if r.match(d[key])]
        return DictList(out)


def get_dict_list(directory="./"):
    """
        Look for files in the given directory and sub-directories
        containing .ndata and .h5 files. Load them lazily and keep
        the json metadata into a list of dictionaries.
        """

    filedata = DictList()
    # get data from .ndata files into dictionaries
    for root, cdir, files in walk(directory):
        for file in files:
            if "ndata" in file:
                dl = root + "/" + file
                raw_data = np.load(dl, mmap_mode="r")
                metadata = json.loads(raw_data["metadata.json"])
                metadata["disk_location"] = dl
                filedata += [
                    metadata,
                ]
            elif "h5" in file:
                dl = root + "/" + file
                f = h5py.File(dl, "r")
                data = f["data"]
                metadata = json.loads(data.attrs["properties"])
                metadata["disk_location"] = dl
                filedata += [
                    metadata,
                ]
    return filedata


def load_nion_file(name, to_hyperspy=True):
    """
    Universal loader for nion data, supporting the .ndata and .h5 formats.
    """

    # process the input depending on the format
    if "h5" in name:
        f = h5py.File(name, "r")
        metadata = json.loads(f["data"].attrs["properties"])
        data = f["data"].value
    elif "ndata" in name:
        f = np.load(name, mmap_mode="r")
        metadata = json.loads(f["metadata.json"])
        data = f["data"]
    elif "json" in name:
        # try to load the data
        data_name = name.replace("json", "npy")
        try:
            data = np.load(data_name)
        except:
            raise ValueError("Numpy npy file not found")
        # and now the metadata
        with open(name, "r") as f:
            s = f.read()
            metadata = json.loads(s)
    else:
        raise ValueError("Only .ndata and .h5 formats are supported!")
    f.close()  # be excellent to each other!

    if to_hyperspy and _hyperspy_support:
        return data_and_metadata_to_hyperspy(data, metadata)
   # elif to_hyperspy:
    #    raise ImportWarning(
     #       "The hyperspy package could not be found."
      #      + "Returning data in numpy array format."
      #  )
    return data, metadata


def data_and_metadata_to_hyperspy(data, metadata):
    """
    Build a hyperspy signal from the numpy data and json metadata extracted
    using `~.load_nion_file`
    """

    if not _hyperspy_support:
        raise ImportError(
            "The hyperspy package could not be found." + "This method is not available."
        )

    # gets the first calibration to pop out of the list
    calibration = next(
        key
        for key in metadata
        if "calibration" in key and key != "intensity_calibration"
    )
    axes = metadata.get(calibration, None)

    # add the data shape
    for ax, Ni in zip(axes, data.shape):
        ax["size"] = Ni

    # get the number of dimensions
    sdim = metadata["datum_dimension_count"]

    # parse the metadata
    parsed_metadata = parse_nion_metadata(metadata)

    if sdim == 1:
        s = Signal1D(data, axes=axes, metadata=parsed_metadata)
    elif sdim == 2:
        s = Signal2D(data, axes=axes, metadata=parsed_metadata)
    else:
        s = BaseSignal(data, axes=axes, metadata=parsed_metadata)

    return s


def parse_nion_metadata(metadata):
    out = {"General": {"title": ""}, "Signal": {"binned": False, "signal_type": ""}}

    # Do the General metadata
    General = out["General"]
    General["title"] = metadata.get("title", None)
    General["created"] = metadata.get("created", None)
    General["modified"] = metadata.get("modified", None)
    General["data_modified"] = metadata.get("data_modified", None)
    General["uuid"] = metadata.get("uuid", None)
    General["original_filename"] = metadata.get("disk_location", None)
    General["time_zone"] = metadata.get("timezone", None)
    General["time_zone_offset"] = metadata.get("timezone_offset", None)
    General["session_id"] = metadata.get("session_id", None)

    # Do the Signal calibration if present
    ic = metadata.get("intensity_calibration", None)
    if ic is not None:
        vlm = {}
        out["Signal"]["quantity"] = ic.get("units", None)
        vlm["gain_factor"] = ic.get("scale", 0.0)
        vlm["gain_offset"] = ic.get("offset", 0.0)
        out["Signal"]["Noise_properties"] = {
            "Variance_linear_model": vlm,
        }

    # parse the rest of the nion metadata, if present
    nion_metadata = metadata.get("metadata", None)
    if nion_metadata is None:
        return out

    known_sources = ["scan_detector", "hardware_source"]
    present_sources = [source for source in known_sources if source in nion_metadata]
    Nsources = len(present_sources)
    if Nsources == 0:
        return out

    # TODO: parse the TEM and EELS dicts following Hyperspy schemes (see below)

    # The first source, either 'scan_detector' or 'hardware_source', is TEM info
    tem_metadata = nion_metadata[present_sources[0]]
    out["Acquisition_instrument"] = {
        "TEM": tem_metadata.get("autostem", None),
    }
    if Nsources < 2:
        return out

    # The second source, if present called 'hardware_source', may be EELS info
    eels_metadata = nion_metadata[present_sources[1]]
    out["Acquisition_instrument"]["TEM"]["EELS"] = eels_metadata.get("autostem", None)
    return out
