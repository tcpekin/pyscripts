import numpy as np
import matplotlib.pyplot as plt
import struct
from tqdm import tqdm
from im_tools import bin_image

plt.ion()


def raw_reader(fname, scan_size, im_size, bin_factor):
    """
    This function will read raw data and appropriately put it in the right shape. The data currently is little endian 16 bit blocks. This data comes from the Zeiss SEM. If saving as block file, remember to convert to 8 bit unsigned ints. This is what ACOM expects.
    """
    data = np.zeros(
        (
            scan_size[0],
            scan_size[1],
            int(im_size[0] / bin_factor),
            int(im_size[1] / bin_factor),
        )
    )
    with open(fname, "rb") as file:
        for i in tqdm(range(scan_size[0])):
            for j in range(scan_size[1]):
                im = file.read(np.prod(im_size) * 2)
                im = struct.unpack("<" + "h" * np.prod(im_size), im)
                im = np.asarray(im)
                im = np.reshape(im, im_size)
                im = bin_image(im, (bin_factor, bin_factor))
                data[i, j, :, :] = im

    return data


im = raw_reader(
    "/media/tom/128GB Pekin/190702_M82881_fusion7/190702105014pos1_1/scan_data.raw",
    (100, 140),
    (1216, 1216),
    4,
)
